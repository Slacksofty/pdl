package pdl.backend;

import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayU8;
import boofcv.struct.image.Planar;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.imageio.ImageIO;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Repository;


@Repository
public class ImageDao implements Dao<Image> {

  private final Map<Long, Image> images = new HashMap<>();

  
  public ImageDao() throws FileNotFoundException{

    ClassPathResource imgFile = new ClassPathResource("images");
    if(!imgFile.exists()){
      throw new FileNotFoundException();
    }
    try {
      filtreDic(imgFile.getFile(),images);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * fonction qui recupere la taille de l'image(img) à l'aide d'un BuffereImage et de la classe Planar
   * et fait appel à la fonction setSize pour afficher le hauteur,la largeur et le nombre
   * de canal de l'image.
   */
  public static void recupSize(BufferedImage b , Image img){
    Planar<GrayU8> image = ConvertBufferedImage.convertFromPlanar(b,null,true,GrayU8.class);
    img.setSize(image.getWidth() + "*" + image.getHeight() + "*" + image.getNumBands());
    
  }

  /**
   * Cette fonction permet de filtre le dossier images pour ne recuperer que les images avec les extensions voulus
   * et qui remplie notre Map avec les images.
   */
  public static void filtreDic (File file , Map<Long, Image> images) {

    final String[] EXTENSIONS = new String[]{
      "jpg", "png"
    };    
    File[] liste = file.listFiles();
      
    for(File item : liste){
      if(item.isFile())
      { 
        for (final String ext : EXTENSIONS) {
          if (item.getName().endsWith("." + ext)) {
            byte[] fileContent;
            try {
              fileContent = Files.readAllBytes(item.toPath());
              Image img = new Image(item.getName(), fileContent);
              BufferedImage input = UtilImageIO.loadImage(item.getPath());
              recupSize(input,img);
              images.put(img.getId(), img);
            } catch (final IOException e) {
              e.printStackTrace();
            }
          }
        }
      } 
      else if(item.isDirectory())
      {
        filtreDic(item, images);
      } 
    }
  }


  @Override
  public Optional<Image> retrieve(final long id) {
    return Optional.ofNullable(images.get(id));
  }

  @Override
  public List<Image> retrieveAll() {
    return new ArrayList<Image>(images.values());
  }

  @Override
  public void create(final Image img) {
    images.put(img.getId(), img);
  }

  @Override
  public void update(final Image img, final String[] params) {
    img.setName(Objects.requireNonNull(params[0], "Name cannot be null"));

    images.put(img.getId(), img);
  }

  @Override
  public void delete(final Image img) {
    images.remove(img.getId());
  }
}
