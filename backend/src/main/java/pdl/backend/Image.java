package pdl.backend;

import boofcv.io.image.UtilImageIO;
import java.awt.image.BufferedImage;
//import boofcv.io.image.UtilImageIO;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;


public class Image {
  private static Long count = Long.valueOf(0);
  private Long id;
  private String name;
  private byte[] data;
  private String size;

  public Image(final String name, final byte[] data) {
    id = count++;
    this.name = name;
    this.data = data;
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public byte[] getData() {
    return data;
  }


  /**
   * fonction qui permet de recuperer le type de l'image à partir de @attribue name 
   */
  public org.springframework.http.MediaType getType(){
    
    int index = name.lastIndexOf('.');
    String type = "";
    if (index > 0)
      type = name.substring(index+1);
    if(type.equals("jpg"))
      return MediaType.IMAGE_JPEG;
    return MediaType.IMAGE_PNG;
  }

 
  public void setSize(String size) {
    this.size = size;
  }

  public String getSize() {
    return size;
  }

}
